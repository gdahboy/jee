package Beans;
import Entity.Event;
import Entity.Product;
import Entity.Rayon;
import ServiceF.CouponsService;
import ServiceF.EventService;
import model.Produit;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.xhtmlrenderer.pdf.ITextRenderer;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import static model.CategorieP.Desserts;

@ManagedBean(name = "eventBean")
@SessionScoped
public class EventBeans implements Serializable {
	private static final long serialVersionUID = 1L;
	private int IdEvent ;
	private String Name ;
	private String imageE;
	private String description;
	private Date dateD;
	private Date dateF;
	private int hours;
	private Date  date_publication;

	private Date  deadline;
	private int nbVue;
	private int nbApply;
	static Event event1=new Event();

	private Product Product ;
	private  int IdProduct ;
	private Double moyenne_rate;
	private long number_rate;
	public int getIdProduct() {
		return IdProduct;
	}

	public void setIdProduct(int idProduct) {
		IdProduct = idProduct;
	}

	public Entity.Product getProduct() {
		return Product;
	}

	public void setProduct(Entity.Product product) {
		Product = product;
	}

	ArrayList<Object> listdata = new ArrayList<Object>();

	@EJB
	CouponsService couponsService ;

	@EJB
	EventService eventService;

	/**************************GetAllEvent**************************************************************/
	@GET
	@javax.ws.rs.Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Object> getAll(){

		//String lr= eventService.consomation();
		String Okhra = couponsService.consomation() ;
		JSONArray array = new JSONArray(Okhra);
		ArrayList<Object> listdata = new ArrayList<Object>();

		if (array != null) {
			for (int i=0;i<array.length();i++){

				listdata.add(array.get(i));

			}
		}
		System.out.print(listdata);
		return listdata;
	}
	static Event event=new Event();

	/**************************Add**************************************************************/

	@POST
	@javax.ws.rs.Produces(MediaType.APPLICATION_JSON)
	public void AddEvent(){


		//couponsService.consomation() ;

		String jsonString = new JSONObject().put("IdEvent", IdEvent).put("Name", Name).put("imageE",imageE).put("description",description).
		put("hours",hours).put("nbApply",nbApply).
		put("nbVue",nbVue).put("deadline",deadline).
		put("IdProduct",38).toString();
		eventService.addOrganizer(jsonString);
		System.out.println(jsonString);
		System.out.println("***********Event Added successfully***********");
		System.out.println(eventService.addOrganizer(jsonString));
	}
	public String Apply1(int id){
		try{
			//event1 = eventService.getById(id);
			//System.out.println(event1);
		//	setNbApply(event1.getNbApply()+1);
		}catch(Exception e){}



		//couponsService.consomation() ;

		return "/xhtml/DetailsForms.xhtml";
	}
	/**************************Apply**************************************************************/

	@POST
	@javax.ws.rs.Produces(MediaType.APPLICATION_JSON)
	public void Apply(int id)
	{
		String lr= eventService.consomationEvent(id);
		JSONObject array = new JSONObject(lr);
	     int g = (Integer) array.get("nbApply");
		String jsonString = new org.primefaces.json.JSONObject()
				.put("IdEvent", this.IdEvent)
				.put("nbApply", g+1)
				.toString();
		eventService.AddApply(jsonString,id);
		System.out.println(jsonString);

	}
	/**************************Delete**************************************************************/

	@DELETE
	public void eventDelete(){
		System.out.println("*********Eve*******");
		eventService.deleteEvent(38);
		System.out.println("****************************");
	}
    /**************************DETAILS**************************************************************/
	@GET
	@javax.ws.rs.Produces(MediaType.APPLICATION_JSON)
	public Object getEventDetail(){
		String lr= eventService.consomationEvent(event1.getIdEvent());
		JSONObject array = new JSONObject(lr);
		return array;
	}

	public String getPageDetails(int eventId){

		event1.setIdEvent(eventId);
		return "/eventDetails?faces-redirect=true";

	}
	/**************************DETAILS**************************************************************/
	public void createPDF(int eventId){
		event1.setIdEvent(eventId);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		String url = "http://localhost:8080/MicroInes-web/pdf.xhtml;JSESSIONID="+session.getId()+"pdf=true";
		try {
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(url);
			renderer.layout();
			HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
			response.reset();
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition","/Users/inesatia/Desktop/first.pdf");
			OutputStream browserStream = response.getOutputStream();
			renderer.createPDF(browserStream);
			browserStream.close();
			session.invalidate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		facesContext.responseComplete();

	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public int getIdEvent() {
		return IdEvent;
	}

	public void setIdEvent(int idEvent) {
		IdEvent = idEvent;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getImageE() {
		return imageE;
	}

	public void setImageE(String imageE) {
		this.imageE = imageE;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateD() {
		return dateD;
	}

	public void setDateD(Date dateD) {
		this.dateD = dateD;
	}

	public Date getDateF() {
		return dateF;
	}

	public void setDateF(Date dateF) {
		this.dateF = dateF;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public Date getDate_publication() {
		return date_publication;
	}

	public void setDate_publication(Date date_publication) {
		this.date_publication = date_publication;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public int getNbVue() {
		return nbVue;
	}

	public void setNbVue(int nbVue) {
		this.nbVue = nbVue;
	}

	public int getNbApply() {
		return nbApply;
	}

	public void setNbApply(int nbApply) {
		this.nbApply = nbApply;
	}



	public ArrayList<Object> getListdata() {
		return listdata;
	}

	public void setListdata(ArrayList<Object> listdata) {
		this.listdata = listdata;
	}

	public EventService getEventService() {
		return eventService;
	}

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	public static Event getEvent() {
		return event;
	}

	public static void setEvent(Event event) {
		EventBeans.event = event;
	}

}
