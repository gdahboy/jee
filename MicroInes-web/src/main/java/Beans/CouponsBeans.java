package Beans;

import ServiceF.CouponsService;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean ;
import javax.enterprise.context.SessionScoped;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;


@ManagedBean(name = "couponsBean")
@SessionScoped
public class CouponsBeans implements Serializable {

    private static final long serialVersionUID = 1L;

    private int idCoupon ;

    private int dateMM ;
    private int dateYY ;
    private int dateJJ ;

    private int dureeMM ;
    private int dureeYY ;
    private int dureeJJ ;

    private String Categorie ;
    private int percontage ;

    public int getDateMM() {
        return dateMM;
    }

    public void setDateMM(int dateMM) {
        this.dateMM = dateMM;
    }

    public int getDateYY() {
        return dateYY;
    }

    public void setDateYY(int dateYY) {
        this.dateYY = dateYY;
    }

    public int getDateJJ() {
        return dateJJ;
    }

    public void setDateJJ(int dateJJ) {
        this.dateJJ = dateJJ;
    }

    public int getDureeMM() {
        return dureeMM;
    }

    public void setDureeMM(int dureeMM) {
        this.dureeMM = dureeMM;
    }

    public int getDureeYY() {
        return dureeYY;
    }

    public void setDureeYY(int dureeYY) {
        this.dureeYY = dureeYY;
    }

    public int getDureeJJ() {
        return dureeJJ;
    }

    public void setDureeJJ(int dureeJJ) {
        this.dureeJJ = dureeJJ;
    }

    public String getCategorie() {
        return Categorie;
    }

    public void setCategorie(String categorie) {
        Categorie = categorie;
    }

    public int getPercontage() {
        return percontage;
    }

    public void setPercontage(int percontage) {
        this.percontage = percontage;
    }

    @EJB
    CouponsService couponsService ;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Object> getAll() {

        String Coupons = couponsService.consomation() ;
        JSONArray array = new JSONArray(Coupons) ;

        ArrayList<Object> list = new ArrayList<>() ;

        if (array != null) {
            for (int i=0 ; i < array.length() ; i++) {
                list.add(array.get(i));
            }
        }

        return list ;

    }

    @Produces
    @POST
    public void AddCoupon() {
        String stringDuree  = "" ;
        String stringDate = "" ;
        // 2020-06-04T12:05:26.033
        stringDuree.concat(String.valueOf(dureeYY)).concat(String.valueOf("-"))
                .concat(String.valueOf(dureeMM)).concat(String.valueOf("-")).concat(String.valueOf(dureeJJ))
                .concat(String.valueOf("T00:00:00.000")) ;

         stringDate.concat(String.valueOf(dateYY)).concat("-")
                .concat(String.valueOf(dateMM)).concat("-").concat(String.valueOf(dateJJ))
                .concat("T00:00:00.000") ;

        String jsonString = new JSONObject().put("idcoupon" , idCoupon).put("etat" , 1)
                .put("percontage" , percontage).put("date" ,(String.valueOf(dateYY)).concat("-")
                        .concat(String.valueOf(dateMM)).concat("-").concat(String.valueOf(dateJJ))
                        .concat("T00:00:00.000") ).put("duree" , (String.valueOf(dureeYY).concat(String.valueOf("-"))).concat(String.valueOf(dureeMM)).concat(String.valueOf("-")).concat(String.valueOf(dureeJJ))
                        .concat(String.valueOf("T00:00:00.000"))).toString() ;

        couponsService.addCoupons(jsonString);

    }
    @DELETE
    @Path("{idcoupon}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void DeleteCoupons(@PathParam("idcoupon") int idcoupon) {
        couponsService.deleteCoupons(idcoupon);
    }
    @PUT
    @Produces
    public void ActivateDeactivate(int id ) {
        this.sendEmail();
        String coupon = couponsService.consomationCoupon(id) ;
        JSONObject Array = new JSONObject(coupon) ;
        int etat = (Integer) Array.get("etat") ;
        if (etat == 0 ) {
            etat =1 ;
        }else {
            if (etat == 1) {
                etat = 0 ;
            }
        }
        String jsonString = Array.put("etat" , etat).toString() ;
        couponsService.modifyCoupons(id , jsonString) ;
    }
    @POST
    @javax.ws.rs.Produces(MediaType.APPLICATION_JSON)
    public void sendEmail(){


        //FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("vous avez reserver"+" "+Quantites+" "+"tickets"));
        final String username = "cyruss.paulino@andyes.net";
        final String password = "asyra4K@";
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }

        });
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("levio"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("iphonegdah@yahoo.fr"));
            message.setSubject("New Task assigned to you");
            message.setText("Dear Mr&Ms, your president assigned you new task ' "+"xdfgchvjk"+" ' please visite your account in ConsultTech to finish your tasks, good job and Best regards,");
            Transport.send(message);
            System.out.println("Was the email : Done");
        } catch (Exception e) {
            // throw new RuntimeException(e);
            System.out.println(e.getMessage());
        }
    }






}
