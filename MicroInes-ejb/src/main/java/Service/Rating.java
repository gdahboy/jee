package Service;

import javax.persistence.*;
import java.io.Serializable;



public class Rating implements Serializable {

	private int idrate;
	
	private int idEvent;

	private int rate;

	public Rating() {
	}

	public int getidEvent() {
		return this.idEvent;
	}

	public void setidEvent(int idEvent) {
		this.idEvent = idEvent;
	}

	public int getIdrate() {
		return this.idrate;
	}

	public void setIdrate(int idrate) {
		this.idrate = idrate;
	}

	public int getRate() {
		return this.rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public Rating(int idEvent, int rate) {
		super();
		this.idEvent = idEvent;
		this.rate = rate;
	}
	
	

}