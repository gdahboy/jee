package Service;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

@Stateless
@LocalBean
public class ProductClient  {


    public void consommation() {
        Client client = ClientBuilder.newClient();

        WebTarget target = client.target("https://10.211.55.7:44326/api/Products");
        WebTarget Hi = target.path("Ines");
        Response response = Hi.request().get();
        String result = response.readEntity(String.class);
        System.out.println(result);
        response.close();
    }
}
