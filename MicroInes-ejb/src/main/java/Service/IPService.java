package Service;



import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Stateless
@LocalBean
public class IPService {
	
	public static final String endpoint = "http://ip-api.com/json";

	public String getProducts(){
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("https://10.211.55.7:44326/api/Products") ;
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		Product[] produits =  response.readEntity(Product[].class);
		System.out.println("Status : " + response.getStatus());
		for (int index = 0; index < produits.length; index++) {
			System.out.println(produits[index]);
		}
		return "hi";

	}

	public String getUsers(){
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("https://jsonplaceholder.typicode.com/posts") ;
		 
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		 
		Post[] posts =  response.readEntity(Post[].class);
		     
		System.out.println("Status : " + response.getStatus());
		
		for (int index = 0; index < posts.length; index++) {
			System.out.println(posts[index]);
		}
		return posts[1].getTitle();
	}


	

}
