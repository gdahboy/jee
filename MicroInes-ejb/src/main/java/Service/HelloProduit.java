package Service;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.client.ClientBuilder.*;

public class HelloProduit {
    public static void main(String[] args) throws NamingException{

        Client client = newClient();
        WebTarget webTarget = client.target("https://10.211.55.7:44326/api/Products") ;
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        Product[] produits =  response.readEntity(Product[].class);
        System.out.println("Status : " + response.getStatus());
        for (int index = 0; index < produits.length; index++) {
            System.out.println(produits[index]);
        }
    }
}
