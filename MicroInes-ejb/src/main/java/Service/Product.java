package Service;

import jdk.nashorn.internal.objects.annotations.Getter;
import model.CategorieP;
import model.Event;
import model.Rayon;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.io.Serializable;
import java.util.Date;

@Path("Products")
public class Product  implements Serializable {
    @GET
    public String Hi1(){
        return "This is the product";
    }
    @Path("{Nom}")
    @GET
    public String Hi(@PathParam(value = "Nom")String name){
        return "This is the product"+name;
    }

    @GET
    public String Hito(@QueryParam(value = "Nom") String name){
        return "This is the product"+name;
    }
}
