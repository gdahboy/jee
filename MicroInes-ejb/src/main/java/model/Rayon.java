package model;

import javax.persistence.*;
import java.util.List;
@Entity
@Table(name="Rayon")
@NamedQuery(name="Rayon.findAll", query="SELECT r FROM Rayon r")
public class Rayon {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int IdRayon;
    @Column(name="description")

    private String description ;
    @Column(name="typeR")

    private  String typeR ;
    @Column(name="imageR")

    private   String imageR ;
    @Column(name="flag")

    private String flag ;
    @OneToMany(mappedBy="rayon")
    private List<Produit> produits;
    public Rayon() {
    }

    public Rayon(List<Produit> produits, int idRayon, String description, String typeR, String imageR, String flag) {
        this.produits = produits;
        IdRayon = idRayon;
        this.description = description;
        this.typeR = typeR;
        this.imageR = imageR;
        this.flag = flag;
    }


    public List<Produit> getProduits() {
        return produits;
    }

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }

    public int getIdRayon() {
        return IdRayon;
    }

    public void setIdRayon(int idRayon) {
        IdRayon = idRayon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTypeR() {
        return typeR;
    }

    public void setTypeR(String typeR) {
        this.typeR = typeR;
    }

    public String getImageR() {
        return imageR;
    }

    public void setImageR(String imageR) {
        this.imageR = imageR;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
