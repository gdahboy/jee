package model;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the Rating database table.
 * 
 */
@Entity
@NamedQuery(name="Rating.findAll", query="SELECT r FROM Rating r")
public class Rating implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idrate;
	
	private int idEvent;

	@Column(name="rate")
	private int rate;

	public Rating() {
	}

	public int getidEvent() {
		return this.idEvent;
	}

	public void setidEvent(int idEvent) {
		this.idEvent = idEvent;
	}

	public int getIdrate() {
		return this.idrate;
	}

	public void setIdrate(int idrate) {
		this.idrate = idrate;
	}

	public int getRate() {
		return this.rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public Rating(int idEvent, int rate) {
		super();
		this.idEvent = idEvent;
		this.rate = rate;
	}
	
	

}