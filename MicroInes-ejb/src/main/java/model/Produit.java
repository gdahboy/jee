package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name="Produit")
@NamedQuery(name="Produit.findAll", query="SELECT p FROM Produit p")
public class Produit  implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int IdProduit ;

    public Produit() {
    }

    @Column(name="Nom")
    private String Nom ;
    @Column(name="imageP")
    private String imageP;
    @Column(name="description")
    private String description ;
    @Enumerated(EnumType.STRING)
    private CategorieP CategorieP;
    @Column(name="barcode")
    private String barcode;
    @Column(name="marque")

    private String marque;
    @Column(name="price")

    private double   price;
    @Column(name="dateC")

    private Date dateC;
    @Column(name="dateF")
    private Date   dateF;
    @Column(name="qte")
    private int qte;

   @ManyToOne
    private Rayon rayon;

    @OneToOne(mappedBy="produit")
    private Event event;

    public Produit(Rayon rayon) {
        this.rayon = rayon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDateC() {
        return dateC;
    }

    public void setDateC(Date dateC) {
        this.dateC = dateC;
    }

    public Date getDateF() {
        return dateF;
    }

    public void setDateF(Date dateF) {
        this.dateF = dateF;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public Rayon getRayon() {
        return rayon;
    }

    public void setRayon(Rayon rayon) {
        this.rayon = rayon;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public int getIdProduit() {
        return IdProduit;
    }

    public void setIdProduit(int idProduit) {
        IdProduit = idProduit;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getImageP() {
        return imageP;
    }

    public void setImageP(String imageP) {
        this.imageP = imageP;
    }

    public model.CategorieP getCategorieP() {
        return CategorieP;
    }

    public void setCategorieP(model.CategorieP categorieP) {
        CategorieP = categorieP;
    }
}
