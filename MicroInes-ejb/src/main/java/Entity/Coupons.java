package Entity;


import java.io.Serializable;
import java.util.Date;

public class Coupons implements Serializable {

    private int idcoupon ;
    private Date durre ;
    private CategorieP categorie ;
    private float percontage ;
    private Date date ;
    private int etat ;
    private int panierId ;

    public Coupons(int idcoupon, Date durre, float percontage, Date date, int etat, int panierId) {
        this.idcoupon = idcoupon;
        this.durre = durre;
        this.percontage = percontage;
        this.date = date;
        this.etat = etat;
        this.panierId = panierId;
    }

    public int getIdcoupon() {
        return idcoupon;
    }

    public void setIdcoupon(int idcoupon) {
        this.idcoupon = idcoupon;
    }

    public Date getDurre() {
        return durre;
    }

    public void setDurre(Date durre) {
        this.durre = durre;
    }

    public CategorieP getCategorie() {
        return categorie;
    }

    public void setCategorie(CategorieP categorie) {
        this.categorie = categorie;
    }

    public float getPercontage() {
        return percontage;
    }

    public void setPercontage(float percontage) {
        this.percontage = percontage;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public int getPanierId() {
        return panierId;
    }

    public void setPanierId(int panierId) {
        this.panierId = panierId;
    }
}
