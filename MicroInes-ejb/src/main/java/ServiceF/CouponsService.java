package ServiceF;



import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import Entity.Coupons ;

@Stateless
@LocalBean
public class CouponsService {




    public String consomation() {
        Client client = ClientBuilder.newClient() ;
        WebTarget target = client.target("http://win:6415/api/CouponApi/") ;
        Response response =  target.request().get();
        String res = response.readEntity(String.class) ;
        System.out.print(res);
        return res ;
    }
    public String consomationCoupon(int id){
        Client client = ClientBuilder.newClient() ;
        WebTarget target = client.target("http://win:6415/api/CouponApi/"+id) ;
        Response response = target.request().get() ;
        String result=response.readEntity(String.class);
        return result;
    }

    public void deleteCoupons(int id ) {
        Client client = ClientBuilder.newClient() ;
        WebTarget target = client.target("http://win:6415/api/CouponApi/"+id) ;
        Invocation.Builder invocation = target.request() ;
        invocation.delete() ;
    }
    public String modifyCoupons(int id  , String s ) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://win:6415/api/CouponApi/"+id);
        Invocation.Builder invocationBuilder = target.request();
        Response response = invocationBuilder.put(Entity.entity(s, MediaType.APPLICATION_JSON));
        return  response.readEntity(String.class) ;

    }
    public  void addCoupons(String coupons ) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://win:6415/api/CouponApi/");
        Invocation.Builder invocationBuilder = target.request();
        Response response = invocationBuilder.post(Entity.entity(coupons, MediaType.APPLICATION_JSON));

    }




}
