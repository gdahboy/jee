package ServiceF;

import org.json.JSONArray;
import org.json.JSONObject;
import Entity.Event;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

@Stateless
@LocalBean
public class EventService implements EventServiceRemote, EventServiceLocal {

    
    public EventService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public String consomation() {



		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("http://10.211.55.7:6415/api/Events");
		
		Response response=target.request().get();
		String result=response.readEntity(String.class);
		
		//response.close();
		
		return result;
	}
	@Override
	public String AddApply(String s,int id) {

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://10.211.55.7:6415/api/Events/"+id);
		Invocation.Builder invocationBuilder = target.request();
		Response response = invocationBuilder.post(Entity.entity(s, MediaType.APPLICATION_JSON));
		return response.readEntity(String.class);
	}
	@Override
	public String consomationPresidentEvent(int id) {
		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("https://10.211.55.7:44326/api/Products/"+id);
		
		Response response=target.request().get();
		String result=response.readEntity(String.class);
		
		//response.close();
		
		return result;
	}

	@Override
	public String consomationEvent(int eventId) {
		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("http://10.211.55.7:6415/api/Events/"+eventId);
		Response response=target.request().get();
		String result=response.readEntity(String.class);
		return result;
	}


	@Override
	public String consomationScheduler(int eventId) {
		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("https://10.211.55.7:44326/api/Products/"+eventId);
		Response response=target.request().get();
		String result=response.readEntity(String.class);
		return result;
	}
	
	@Override
	public String consomationEventLikers(int eventId) {
    	int like=0;
		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("http://webapicontroller1-dev.eu-west-1.elasticbeanstalk.com/api/Event/GetEventLikers/"+eventId);
		
		Response response=target.request().get();
		String result=response.readEntity(String.class);
		JSONArray array = new JSONArray(result);
     
        
        if (array != null) { 
            for (int i=0;i<array.length();i++){ 
            	
            	JSONObject object = array.getJSONObject(i);
            	if(object.getInt("status")==1){
            		like=like+1;
            	System.out.println(like);
            	}
             
            } 
         }
		
		return result;
	}

	@Override
	public int consomationParticipantNumber(int eventId) {
		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("http://webapicontroller1-dev.eu-west-1.elasticbeanstalk.com/api/Event/GetParticipantNumber/"+eventId);
		
		Response response=target.request().get();
		int result=response.readEntity(int.class);
		
		//response.close();
		
		return result;
	}

	@Override
	public String consomationOrganizerTasks(int eventId) {
		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("http://webapicontroller1-dev.eu-west-1.elasticbeanstalk.com/api/Event/GetOrganizerTasks/"+eventId);
		
		Response response=target.request().get();
		String result=response.readEntity(String.class);
		
		//response.close();
		
		return result;
	}

	@Override
	public String consomationOrganizerEmail(int organizerID) {
		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("http://webapicontroller1-dev.eu-west-1.elasticbeanstalk.com/api/Event/GetOrganizerEmail/"+organizerID);
		
		Response response=target.request().get();
		String result=response.readEntity(String.class);
		
		//response.close();
		
		return result;
	}

	@Override
	public void deleteEvent(int eventID) {
		Client client=ClientBuilder.newClient();
		client.target("http://10.211.55.7:6415/api/Products/"+eventID);
	}

	@Override
	public String addOrganizer(String Event) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://10.211.55.7:6415/api/Events/PostEvent");
		Invocation.Builder invocationBuilder = target.request();
		Response response = invocationBuilder.post(Entity.entity(Event, MediaType.APPLICATION_JSON));
		return response.readEntity(String.class);
	}

	@Override
	public int getOrganizerId(String email) {
		Client client=ClientBuilder.newClient();
		WebTarget target=client.target("http://webapicontroller1-dev.eu-west-1.elasticbeanstalk.com/api/Event/GetOrganizerId?email="+email);
		
		Response response=target.request().get();
		int result=response.readEntity(int.class);
		
		//response.close();
		
		return result;
	}
	

}
